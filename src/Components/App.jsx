import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Redirect,
} from 'react-router-dom';
import Login from './Login';
import Register from './Register';
import Navigation from './Navigation';
import { Trials } from './Trials';
import { UserEdit } from "./User";
import { Authenticator, AuthUserContext } from './Util';
import './App.scss';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: Authenticator.getUser(),
    };
  }

  updateUser(user) {
    this.setState({ user });
  }

  logOut() {
    this.setState({ user: null }, () => Authenticator.removeUser());
  }

  render() {
    const { user } = this.state;
    return (
      <AuthUserContext.Provider value={user}>
        <div className="app-background">
          <Router>
            <div className="center">
              <Navigation logOutCallback={() => this.logOut()} />
              <div className="main">
                <Route path="/" exact>
                  <h2>Internet Court of Arbitration</h2>
                  <p>Litigate your mates</p>
                </Route>
                <Route path="/trials">
                  { user ? <Trials /> : <Redirect to="/login" /> }
                </Route>
                <Route path="/me">
                  { user ? <UserEdit user={user.user} successCallback={this.updateUser.bind(this)} /> : <Redirect to="/login" /> }
                </Route>
                <Route path="/login">
                  <Login successCallback={this.updateUser.bind(this)} />
                </Route>
                <Route path="/register">
                  <Register successCallback={this.updateUser.bind(this)} />
                </Route>
              </div>
            </div>
          </Router>
        </div>
      </AuthUserContext.Provider>
    );
  }
}

export default App;
