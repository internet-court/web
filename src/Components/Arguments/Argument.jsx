import React from 'react';
import PropTypes from 'prop-types';
import './Arguments.scss';

class Argument extends React.Component {
  render() {
    const { argument, type, picUrl } = this.props;
    const urlSet = picUrl !== null && picUrl !== '';
    const style = urlSet ? { 'background-image': `url(${picUrl})` } : {};
    return (
      <div className={`argument ${type}`}>
        <div className="round-image" style={style}>
        </div>
        <div>
          <p><strong>{argument.Author.UserName}</strong></p>
          <p>{argument.Text}</p>
        </div>
      </div>
    );
  }
}

Argument.propTypes = {
  argument: PropTypes.object.isRequired,
  picUrl: PropTypes.string
};

export default Argument;
