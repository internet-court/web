import React from 'react';
import PropTypes from 'prop-types';
import { AuthUserContext } from '../Util';

class ArgumentEditor extends React.Component {
  constructor() {
    super();
    this.state = {
      argument: null,
    };
  }

  handleInput(key, event) {
    this.setState({ [key]: event.target.value });
  }

  isParticipant(userId) {
    const { defendantId, plaintiffId } = this.props;
    return userId === defendantId || userId === plaintiffId;
  }

  editor(argument, saveCallback) {
    return (
      <div>
        <textarea defaultValue={argument} placeholder="Write an argument" onChange={(e) => this.handleInput('argument', e)} />
        <button onClick={() => saveCallback(argument)}>Save</button>
      </div>
    );
  }
  render() {
    const { saveCallback } = this.props;
    const { argument } = this.state;
    return (
      <AuthUserContext.Consumer>
        { (user) => (user
          ? this.isParticipant(user.user.ID)
            ? this.editor(argument, saveCallback)
            : null
          : null) }
      </AuthUserContext.Consumer>
    );
  }
}

ArgumentEditor.propTypes = {
  defendantId: PropTypes.number.isRequired,
  plaintiffId: PropTypes.number.isRequired,
  saveCallback: PropTypes.func.isRequired,
};

export default ArgumentEditor;
