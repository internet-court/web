import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { http, Authenticator } from '../Util';

const SUCCESS_REDIRECT = '/trials';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      message: '',
    };
  }

  attemptLogIn() {
    const { message, ...request } = this.state;
    const { successCallback } = this.props;
    http.post('/login', request)
      .then((response) => {
        Authenticator.setUser(response.data);
        successCallback(response.data);
      })
      .catch(() => {
        Authenticator.removeUser();
      });
  }

  handleInput(key, event) {
    this.setState({ [key]: event.target.value });
  }

  render() {
    const { username, password } = this.state;
    return Authenticator.isLoggedIn()
      ? <Redirect to={SUCCESS_REDIRECT} />
      : (
        <div className="login-form form">
          <div className="row">
            <label htmlFor="username">Username</label>
            <input id="username" type="text" name="username" value={username} onChange={(e) => this.handleInput('username', e)} />
          </div>
          <div className="row">
            <label htmlFor="password">Password</label>
            <input id="password" type="password" name="Pasword" value={password} onChange={(e) => this.handleInput('password', e)} />
          </div>
          <div className="row buttons">
            <button onClick={() => this.attemptLogIn()} type="submit">Log In</button>
          </div>
        </div>
      );
  }
}

Login.propTypes = {
  successCallback: PropTypes.func.isRequired,
};

export default Login;
