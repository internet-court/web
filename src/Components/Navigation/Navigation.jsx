import React from 'react';
import PropTypes from 'prop-types';
import { AuthUserContext } from '../Util';
import NavigationAuth from './NavigationAuth';
import NavigationPublic from './NavigationPublic';
import './Navigation.scss';

class Navigation extends React.Component {
  render() {
    const { logOutCallback } = this.props;
    return (
      <AuthUserContext.Consumer>
        { (user) => (user
          ? <NavigationAuth logOutCallback={logOutCallback} />
          : <NavigationPublic />) }
      </AuthUserContext.Consumer>
    );
  }
}

Navigation.propTypes = {
  logOutCallback: PropTypes.func.isRequired,
};

export default Navigation;
