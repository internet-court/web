import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class NavigationAuth extends React.Component {
  render() {
    const { logOutCallback } = this.props;
    return (
      <nav className="menu menu-auth">
        <h2>Internet Court</h2>
        <div className="line" />
        <div className="links">
          <Link to="/">Home</Link>
          <Link to="/trials">Trials</Link>
          <Link to="/me">Account</Link>
          <button type="submit" onClick={logOutCallback}>Log Out</button>
        </div>
      </nav>
    );
  }
}

NavigationAuth.propTypes = {
  logOutCallback: PropTypes.func.isRequired,
};

export default NavigationAuth;
