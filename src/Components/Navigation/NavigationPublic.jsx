import React from 'react';
import { Link } from 'react-router-dom';

class NavigationPublic extends React.Component {
  render() {
    return (
      <nav className="menu">
        <h2>Internet Court</h2>
        <div className="links">
          <Link to="/">Home</Link>
          <Link to="/login">Login</Link>
          <Link to="/register">Register</Link>
        </div>
      </nav>
    );
  }
}

export default NavigationPublic;
