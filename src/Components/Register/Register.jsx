import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { http, Authenticator } from '../Util';

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: '',
      last_name: '',
      username: '',
      email: '',
      password: '',
      confirm_password: '',
      errors: '',
    };
  }

  handleChange(key, event) {
    this.setState({ [key]: event.target.value });
  }

  checkPasswords() {
    const { password, confirm_password } = this.state;
    if (password !== confirm_password) {
      this.setState({ errors: 'Passwords do not match!' });
    } else {
      this.setState({ errors: '' });
    }
  }

  handlePasswordChange(key, event) {
    this.setState({ [key]: event.target.value }, this.checkPasswords);
  }

  attemptRegister() {
    // somits errors and confirmpassword
    const { errors, confirm_password, ...request } = this.state;
    const { successCallback } = this.props;
    http.post('/register', request)
      .then((response) => {
        Authenticator.setUser(response.data);
        successCallback(response.data);
      })
      .catch(() => {
        Authenticator.removeUser();
      });
  }

  render() {
    const {
      errors, first_name, last_name, username, email, password, confirm_password,
    } = this.state;
    return Authenticator.isLoggedIn()
      ? <Redirect to="/private" />
      : (
        <div className="register-form form">
          <p>
            Errors:
            {errors}
          </p>
          <div className="row">
            <label htmlFor="firstname">First Name</label>
            <input
              type="text"
              name="firstname"
              value={first_name}
              onChange={(e) => this.handleChange('first_name', e)}
            />
          </div>
          <div className="row">
            <label htmlFor="lastname">Last Name</label>
            <input
              type="text"
              name="lastname"
              value={last_name}
              onChange={(e) => this.handleChange('last_name', e)}
            />
          </div>
          <div className="row">
            <label htmlFor="username">Username</label>
            <input
              type="text"
              name="username"
              value={username}
              onChange={(e) => this.handleChange('username', e)}
            />
          </div>
          <div className="row">
            <label htmlFor="email">Email</label>
            <input
              type="email"
              name="email"
              value={email}
              onChange={(e) => this.handleChange('email', e)}
            />
          </div>
          <div className="row">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              name="password"
              value={password}
              onChange={(e) => this.handlePasswordChange('password', e)}
            />
          </div>
          <div className="row">
            <label htmlFor="confirmPassword">Confirm Password</label>
            <input
              type="password"
              name="confirmPassword"
              value={confirm_password}
              onChange={(e) => this.handlePasswordChange('confirm_password', e)}
            />
          </div>
          <div className="row buttons">
            <button type="submit" onClick={() => this.attemptRegister()}>Register</button>
          </div>
        </div>
      );
  }
}

Register.propTypes = {
  successCallback: PropTypes.func.isRequired,
};

export default Register;
