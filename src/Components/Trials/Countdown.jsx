import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

class Countdown extends React.Component {
  constructor() {
    super();
    this.state = {
      diff: moment.duration(),
    };
  }

  componentDidMount() {
    const { expiry } = this.props;
    const end = moment(expiry, 'YYYY-MM-DDThh:mm:ssZ');
    this.interval = setInterval(() => {
      const now = moment();
      const diff = moment.duration(end.diff(now));

      this.setState({
        diff,
      });
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  durationString() {
    const { diff } = this.state;
    if (diff.seconds() <= 0) {
      return 'Ended';
    }
    if (diff.days() > 0) {
      return `Ends in ${diff.days()} days`;
    } else if (diff.hours() > 0) {
      return `Ends in ${diff.hours()}:${diff.minutes()}:${diff.seconds()}`;
    } else if (diff.minutes() > 0) {
      return `Ends in ${diff.minutes()}:${diff.seconds()}`;
    } else {
      return `Ends in ${diff.seconds()} seconds`;
    }
  }

  render() {
    return (
      <span className="countdown">
        {this.durationString()}
      </span>
    );
  }
}

Countdown.propTypes = {
  expiry: PropTypes.string.isRequired,
};

export default Countdown;
