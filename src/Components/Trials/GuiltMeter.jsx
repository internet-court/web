import React from 'react';
import PropTypes from 'prop-types';
import { http, error } from '../Util';
import './GuiltMeter.scss';

class GuiltMeter extends React.Component {
  constructor() {
    super();
    this.state = {
      percentGuilt: 0,
    };
  }

  componentDidMount() {
    const { trialId } = this.props;
    const url = `/api/trial/${trialId}/votes`;
    http.get(url)
      .then((response) => {
        const guilt = response.data.reduce(this.guiltyAccumulator, 0);
        const percentGuilt = Math.floor((guilt / response.data.length) * 100);
        this.setState({ percentGuilt });
      })
      .catch((err) => error(err));
  }

  guiltyAccumulator(guilt, vote) {
    return vote.Guilty === true ? guilt + 1 : guilt;
  }

  render() {
    const { percentGuilt } = this.state;
    const guiltStyle = {
      width: `${percentGuilt}%`,
    };
    const innocentStyle = {
      width: `${100 - percentGuilt}%`,
    };
    return (
      <div className="meter-wrapper">
        <div className="guilty-percentage" style={guiltStyle} >
          <span>
            {percentGuilt}
            % Guilty
          </span>
        </div>
        <div className="innocent-percentage" style={innocentStyle}>
          <span>
            {100 - percentGuilt}
            % Not Guilty
          </span>
        </div>
      </div>
    );
  }
}

GuiltMeter.propTypes = {
  trialId: PropTypes.number.isRequired,
};

export default GuiltMeter;
