import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

class TrialBox extends React.Component {
  render() {
    const { trial } = this.props;
    const trialUrl = `/trials/${trial.ID}`;
    return (
      <Link to={trialUrl} className="trial-box">
        <p className="title">{trial.Title}</p>
        <p>
          {trial.Plaintiff.UserName}
          {' '}
          v
          {' '}
          {trial.Defendant.UserName}
        </p>
      </Link>
    );
  }
}

TrialBox.propTypes = {
  trial: PropTypes.object.isRequired,
};

export default TrialBox;
