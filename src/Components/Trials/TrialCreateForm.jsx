import React from 'react';
import { Redirect } from 'react-router-dom';
import { UserSelect } from '../User';
import { error, http } from '../Util';

class TrialCreateForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      defendant: null,
      title: '',
      argument: '',
      redirect: false,
    };
  }

  handleInput(key, event) {
    this.setState({ [key]: event.target.value });
  }

  handleChange(key, val) {
    this.setState({ [key]: val });
  }

  generateRequestBody() {
    const { defendant, argument, title } = this.state;
    return {
      title,
      defendantId: defendant.ID,
      arguments: [argument],
    };
  }

  redirect(val) {
    this.setState({redirect: val});
  }

  save() {
    const requestBody = this.generateRequestBody();
    http.post('/api/trial', requestBody)
      .then(response => {
        this.setState({ redirect: true });
      })
      .catch(error);
  }

  render() {
    const { title, argument, redirect } = this.state;
    if (redirect) {
      return <Redirect to="/trials" />;
    }
    return (
      <div className="trial-form">
        <div className="form-grid">
          <label htmlFor="defendant">Defendant</label>
          <UserSelect onPublish={(user) => this.handleChange('defendant', user)} name="user-select" />
          <label htmlFor="title">Title</label>
          <input type="text"
                 name="title"
                 value={title}
                 onChange={(e) => this.handleInput('title', e)} />
          <label htmlFor="opening-argument">Opening Argument</label>
          <textarea name="opening-argument" onChange={(e) => this.handleInput('argument', e)} defaultValue={argument} />
        </div>
        <div className="button-row">
          <button type="button" onClick={() => this.redirect(true)}>Back</button>
          <button type="submit" onClick={() => this.save()}>Lodge Proceedings</button>
        </div>
      </div>
    );
  }
}

export default TrialCreateForm;