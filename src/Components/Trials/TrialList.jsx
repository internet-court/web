import React from 'react';
import PropTypes from 'prop-types';
import TrialBox from './TrialBox';
import './Trials.scss';

class TrialList extends React.Component {
  render() {
    const { trials } = this.props;
    return (
      <div className="trials-list">
        {trials.map((trial) => <TrialBox key={`trial-${trial.ID}`} trial={trial} />)}
      </div>
    );
  }
}

TrialList.propTypes = {
  trials: PropTypes.array.isRequired,
};

export default TrialList;
