import React from 'react';
import { NavLink } from 'react-router-dom';

class TrialMenu extends React.Component {
  render() {
    return (
      <div className="header-wrapper">
        <div className="even-row">
          <h2>Trials</h2>
          <div className="tab-group">
            <NavLink exact to="/trials/new">New</NavLink>
            <NavLink exact to="/trials">All</NavLink>
            <NavLink exact to="/trials/mine">Mine</NavLink>
          </div>
        </div>
      </div>
    );
  }
}

export default TrialMenu;
