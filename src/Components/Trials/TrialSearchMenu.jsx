import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { http } from '../Util';

class TrailSearchMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      query: null,
    };
  }

  componentDidMount() {
    this.fetchTrials();
  }

  generateRequest() {
    const { query } = this.state;
    const { userOnly } = this.props;
    const request = {
      params: { },
    };
    if (query !== null && query !== '') {
      request.params.query = query;
    }
    if (userOnly) {
      request.params.userOnly = 1;
    }
    return request;
  }

  fetchTrials() {
    const { successCallback, failureCallback } = this.props;
    http.get('/api/trial', this.generateRequest())
      .then((response) => {
        successCallback({ trials: response.data });
      })
      .catch(() => {
        failureCallback({ error: 'Failed to get trials' });
      });
  }

  handleInput(key, event) {
    this.setState({ [key]: event.target.value });
  }

  render() {
    return (
      <div className="header-wrapper">
        <div className="even-row">
          <h2>Trials</h2>
          <div className="tab-group">
            <NavLink exact to="/trials/new">New</NavLink>
            <NavLink exact to="/trials">All</NavLink>
            <NavLink exact to="/trials/mine">Mine</NavLink>
          </div>
        </div>
        <div className="even-row">
          <input
            type="text"
            onChange={(e) => this.handleInput('query', e)}
          />
          <button
            type="submit"
            onClick={() => this.fetchTrials()}
          >
            Search
          </button>
        </div>
      </div>
    );
  }
}

TrailSearchMenu.propTypes = {
  successCallback: PropTypes.func.isRequired,
  failureCallback: PropTypes.func.isRequired,
  userOnly: PropTypes.bool.isRequired,
};

export default TrailSearchMenu;
