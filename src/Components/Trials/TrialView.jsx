import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { debug, error, http } from '../Util';
import { Argument, ArgumentEditor } from '../Arguments';
import { Vote } from '../Vote';
import Countdown from './Countdown';
import GuiltMeter from './GuiltMeter';

class TrialView extends React.Component {
  constructor() {
    super();
    this.state = {
      trial: null,
    };
  }

  componentDidMount() {
    this.fetchTrial();
  }

  fetchTrial() {
    const { trialId } = this.props;
    const trialUrl = `/api/trial/${trialId}`;
    http.get(trialUrl)
      .then((response) => this.setState({ trial: response.data }))
      .catch((err) => error(err));
  }

  isParticipant(userId) {
    const { trial } = this.state;
    return userId === trial.Plaintiff.ID || userId === trial.Defendant.ID;
  }

  saveArgument(argument) {
    const { trialId } = this.props;
    const trialArgumentUrl = `/api/trial/${trialId}/argument`;
    http.post(trialArgumentUrl, { argument })
      .then(() => this.fetchTrial()) // should I just add to argument list? or load all again?
      .catch((err) => error(err));
  }

  saveGuilt(trialId, guilt) {
    const url = '/api/vote';
    const body = {
      trialId,
      guilty: guilt,
    };
    http.post(url, body)
      .then((response) => {
        debug(response);
        this.fetchTrial();
      })
      .catch((response) => { error(response); });
  }

  argumentsList() {
    const { trial } = this.state;
    return trial.Arguments.map((argument) => {
      const isPlaintiff = argument.Author.ID === trial.Plaintiff.ID;
      const type = isPlaintiff ? 'plaintiff' : 'defendant';
      const picUrl = isPlaintiff ? trial.Plaintiff.ProfilePicture.PublicUrl : trial.Defendant.ProfilePicture.PublicUrl;
      return <Argument key={argument.ID} argument={argument} type={type} picUrl={picUrl} />;
    });
  }

  render() {
    const { trial } = this.state;
    if (trial === null) {
      return <div className="trial-view-wrapper" />;
    }
    return (
      <div className="trial-view-wrapper">
        <div className="header-wrapper">
          <div className="even-row">
            <h2>{trial.Title}</h2>
            <div>
              <NavLink className="button" to="/trials">Trials</NavLink>
            </div>
          </div>
          <div className="even-row pb-8">
            <div className="participants">
              <span>{trial.Plaintiff.UserName}</span>
              <span> vs </span>
              <span>{trial.Defendant.UserName}</span>
            </div>
            <Countdown expiry={trial.Expiry} />
          </div>
          <div className="even-row">
            <GuiltMeter trialId={trial.ID} />
          </div>
        </div>
        {this.argumentsList()}
        <ArgumentEditor
          plaintiffId={trial.Plaintiff.ID}
          defendantId={trial.Defendant.ID}
          saveCallback={(arg) => this.saveArgument(arg)}
        />
        <Vote
          trial={trial}
          saveCallback={(trialId, guilt) => this.saveGuilt(trialId, guilt)}
        />
      </div>
    );
  }
}

TrialView.propTypes = {
  trialId: PropTypes.string.isRequired,
};

export default TrialView;
