import React from 'react';
import { Route } from 'react-router-dom';
import TrialList from './TrialList';
import TrialSearchMenu from './TrialSearchMenu';
import TrialMenu from './TrialMenu';
import TrialCreateForm from './TrialCreateForm';
import TrialView from './TrialView';
import './Trials.scss';

class Trials extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      trials: [],
      error: null,
    };
  }

  searchCallback(state) {
    this.setState(state);
  }

  render() {
    const { trials, error } = this.state;
    return (
      <div className="trial-list-container">
        <Route
          path="/trials"
          exact
          render={() => (
            <div className="trial-sub-route-container">
              <TrialSearchMenu
                successCallback={(state) => this.searchCallback(state)}
                failureCallback={(state) => this.searchCallback(state)}
                userOnly={false}
              />
              <TrialList trials={trials} error={error} />
            </div>
          )}
        />
        <Route
          path="/trials/mine"
          exact
          render={() => (
            <div className="trial-sub-route-container">
              <TrialSearchMenu
                successCallback={(state) => this.searchCallback(state)}
                failureCallback={(state) => this.searchCallback(state)}
                userOnly
              />
              <TrialList trials={trials} error={error} />
            </div>
          )}
        />
        <Route
          path="/trials/new"
          exact
          render={() => (
            <div className="trial-sub-route-container">
              <TrialMenu />
              <TrialCreateForm />
            </div>
          )}
        />
        <Route
          path="/trials/:id([0-9]*)"
          exact
          render={(props) => {
            const { match } = props;
            return <TrialView trialId={match.params.id} />;
          }}
        />
      </div>
    );
  }
}

export default Trials;
