import React from 'react';
import { error, debug, http } from '../Util';

class UserEdit extends React.Component {
  constructor() {
    super();
    this.state = {
      first_name: '',
      last_name: '',
      email: '',
      id: '',
      username: '',
      imageSrc: '',
      file: null,
    };
  }

  componentDidMount() {
    this.fetchUser();
  }

  handleChange(key, event) {
    this.setState({ [key]: event.target.value });
  }


  fetchUser() {
    http.get('/api/self')
      .then((response) => {
        const user = response.data;
        debug(user);
        this.setState({
          first_name: user.first_name,
          last_name: user.last_name,
          email: user.Email,
          id: user.ID,
          username: user.UserName,
        });
        if (user.ProfilePicture.PublicUrl != null) {
          this.setState({
            imageSrc: user.ProfilePicture.PublicUrl
          });
        }
      }).catch((err) => {
        error(err);
      });
  }

  createFormData() {
    const data = new FormData();
    const { imageSrc, ...body } = this.state;
    for (const key in body) {
      data.append(key, body[key]);
    }
    for (const pair of data.entries()) {
      console.debug({key: pair[0], value: pair[1]});
    }
    return data;
  }


  save() {
    const body = this.createFormData();
    http.post('/api/self', body)
      .then((response) => {
        debug(response);
      })
      .catch((err) => {
        error(err);
      });
  }

  displayImage(uploadEvent) {
    const file = uploadEvent.target.files[0];
    this.setState({ file, imageSrc: URL.createObjectURL(file) });
  }

  render() {
    const {
      first_name,
      last_name,
      username,
      email,
      imageSrc,
    } = this.state;
    return (
      <form className="even-row">
        <div className="col">
          <div className="row">
            <label htmlFor="firstname">First Name</label>
            <input
              type="text"
              name="firstname"
              value={first_name}
              onChange={(e) => this.handleChange('first_name', e)}
            />
          </div>
          <div className="row">
            <label htmlFor="lastname">Last Name</label>
            <input
              type="text"
              name="lastname"
              value={last_name}
              onChange={(e) => this.handleChange('last_name', e)}
            />
          </div>
          <div className="row">
            <label htmlFor="username">Username</label>
            <input
              type="text"
              name="username"
              value={username}
              onChange={(e) => this.handleChange('username', e)}
            />
          </div>
          <div className="row">
            <label htmlFor="email">Email</label>
            <input
              type="email"
              name="email"
              value={email}
              onChange={(e) => this.handleChange('email', e)}
            />
          </div>
          <div className="row image-row">
            <div className="image-form-el">
              <label htmlFor="image">Image</label>
              <input type="file" name="image" onChange={(event) => this.displayImage(event)} />
            </div>
          </div>
          <div className="row buttons">
            <button type="button" onClick={() => this.save()}>Save</button>
          </div>
        </div>
        <div className="col">
          <div className="image-row">
            <div className="image-display">
              <img id="image" src={imageSrc} alt={`${username}'s profile picture`}  />
            </div>
          </div>
        </div>
      </form>
    );
  }
}

export default UserEdit;
