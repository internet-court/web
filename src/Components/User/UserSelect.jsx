import React from 'react';
import PropTypes from 'prop-types';
import { http } from "../Util";
import "./UserSelect.scss";

class UserSelect extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      query: '',
      hide: false,
      users: [],
    };
  }

  publish(user) {
    const { onPublish } = this.props;
    const state = {
      query: user.UserName,
      hide: false,
      users: [],
    };
    this.setState(state);
    onPublish(user);
  }

  hide() {
    this.setState({ hide: true });
  }

  handleInput(key, event, cb = null) {
    this.setState({ [key]: event.target.value }, cb);
  }

  search() {
    const { query } = this.state;
    const getParameters = {
      params: null,
    };
    if (query !== null && query !== '') {
      getParameters.params = { query: query }
    }
    http.get("/api/user", getParameters)
      .then((response) => {
        this.setState({
          users: response.data ? response.data : [],
          hide: false
        })
      });
  }

  userList() {
    const { hide, users } = this.state;
    if (hide === false) {
      return users.map(user =>
        <div
          key={user.ID}
          className="user-select-row"
          onMouseDown={() => this.publish(user)}>
          {user.UserName}
        </div>);
    }
  }

  render() {
    const { query } = this.state;
    const { name } = this.props;
    const usersList = this.userList();
    return (
      <div className="user-select-wrapper" onBlur={() => this.hide()}>
        <input
          type="text"
          className="user-select-input"
          name={name}
          onFocus={() => this.search()}
          onChange={(e) => this.handleInput('query', e, this.search)}
          value={query}
        />
        <div className="user-select-results">
          {usersList}
        </div>
      </div>
    );
  }
}

UserSelect.propTypes = {
  onPublish: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
}

export default UserSelect;