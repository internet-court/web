const TOKEN_KEY = 'IC-TOKEN';

class Authenticator {
  static setUser(user) {
    localStorage.setItem(TOKEN_KEY, JSON.stringify(user));
  }

  static getUser() {
    return this.isLoggedIn() ? JSON.parse(localStorage.getItem(TOKEN_KEY)) : null;
  }

  static removeUser() {
    localStorage.removeItem(TOKEN_KEY);
  }

  static isLoggedIn() {
    return localStorage.getItem(TOKEN_KEY) !== null;
  }

  static isNotLoggedIn() {
    return !Authenticator.isLoggedIn();
  }
}

export default Authenticator;
