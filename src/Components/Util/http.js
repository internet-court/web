import axios from 'axios';
import Authenticator from './auth';

const http = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL,
});

http.interceptors.request.use((config) => {
  const authToken = Authenticator.getUser();
  const authHasToken = authToken !== null && Object.prototype.hasOwnProperty.call(authToken, 'token');
  const configuration = config;
  if (authHasToken) {
    configuration.headers.Authorization = `Bearer ${authToken.token}`;
  }
  return configuration;
});

export default http;
