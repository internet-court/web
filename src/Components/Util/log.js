function debug(message) {
  if(process.env.REACT_APP_DEBUG) {
    console.debug(message);
  }
}

function log(message) {
  if(process.env.REACT_APP_DEBUG) {
    console.log(message);
  }
}

function error(message) {
  console.error(message);
}

export { debug, log, error };