import React from 'react';
import PropTypes from 'prop-types';
import { AuthUserContext } from '../Util';

class Vote extends React.Component {

  isNotParticipant(userId) {
    const { trial } = this.props;
    return !(userId === trial.Defendant.ID || userId === trial.Plaintiff.ID);
  }

  youVotedFor(votes, userId) {
    const vote = votes.find((aVote) => aVote.VoterId === userId ? aVote : null);
    if (vote != null) {
      return (
        <p>
          You voted:
          {vote.Guilty === true ? ' GUILTY!' : ' NOT GUILTY!'}
        </p>
      );
    }
    return null;
  }

  voteButtons(userId) {
    const { trial, saveCallback } = this.props;
    const defendantName = trial.Defendant.UserName;
    return (
      <div className="vote">
        <p>{`Is ${defendantName} guilty?`}</p>
        <button type="button" onClick={() => saveCallback(trial.ID, true)}>
          Guilty
        </button>
        <button type="button" onClick={() => saveCallback(trial.ID, false)}>
          Not Guilty
        </button>
        {this.youVotedFor(trial.Votes, userId)}
      </div>
    );
  }

  render() {
    return (
      <AuthUserContext.Consumer>
        {
          (user) => (user
            ? this.isNotParticipant(user.user.ID)
              ? this.voteButtons(user.user.ID)
              : null
            : null)
        }
      </AuthUserContext.Consumer>
    );
  }
}

Vote.propTypes = {
  trial: PropTypes.object.isRequired,
  saveCallback: PropTypes.func.isRequired,
};

export default Vote;